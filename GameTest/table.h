#include <vector>
#pragma once
#include "Vector2.h"
//class CVector2;

enum LineType
{
	eLine_Hard = 0,
	eLine_Soft,
	eLine_Power,
	eLine_Flipper,
	eLine_Flip,
	eLine_Score,
	eLine_1Hit,
	eLine_2Hit,
	eLine_3Hit,
	eLine_END
};

class CPoint
{
public:
	CPoint();
	CPoint(float x, float y);

	float DistanceToPoint(float x, float y);
	bool IsOnPoint(float x, float y, float tolerance);

	operator CVector2() const;

public:
	float m_x;
	float m_y;
	bool m_selected;
};

class CLineSegment
{
public:
	CLineSegment();
	CLineSegment(float x1, float y1, float x2, float y2, LineType type);
	float DistanceToLine(float x, float y);
	bool IsOnLine(float x, float y, float tolerance = 5.0f);
public:
	CPoint m_start;
	CPoint m_end;
	LineType m_type;
	bool m_selected;
};

typedef void (*LineHitCallback)(CLineSegment&);

class CLineDefinition
{
public:
	CLineDefinition(float restitution = 0.75f);
	CLineDefinition(const char* name, float r, float g, float b, float restitution = 0.75f);

public:
	LineHitCallback m_hitCallback;

	float m_restitution;

	float m_Red;
	float m_Green;
	float m_Blue;
	const char* m_name;
};

class CFlipper
{
public:
	CFlipper(float x = 0.0f, float y = 0.0f, float length = 40.0f, bool isRightFlipper = false, bool isInverted = false);
	CFlipper(CPoint position, float length, bool isRightFlipper, bool isInverted);

	void RecalculateAngles();
	void RecalculateAngles(bool isRightFlipper, bool isInverted);

	float GetSpeedAtPoint(CVector2 point) const;
	// DO NOT USE THIS. IT'S BROKEN AS HECK
	CVector2 GetVelocityAtPoint(CVector2 point) const;
	CVector2 GetEndpoint() const;
	CVector2 GetEndpoint(float angle) const;
	CLineSegment GetLineSegment() const;
public:
	CPoint m_position;
	float m_length;

	bool m_isInverted;

	float m_angle;
	float m_downAngle;
	float m_upAngle;
	int m_flipDirection;
	float m_speed;
	bool m_isRightFlipper;
	bool m_isSelected;
};

// To hold a set of lines describing the background for the game
class CTable
{
public:
	CTable();
public:
	CVector2 m_spawnPosition;

	std::vector<CLineSegment> m_lines;
	CLineDefinition m_lineDefs[eLine_END];
	std::vector<CFlipper> m_flippers;
	std::vector<CLineSegment> m_stationaryFlippers;
};



