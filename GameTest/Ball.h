#pragma once
#include "Vector2.h"

class CLineSegment;
class CFlipper;

class CBall
{
public:
	CBall(CVector2 position, float radius = 8.0f, CVector2 gravity = CVector2(0.0f, -600.0f));
	~CBall();

	void Update(float deltaTime);
public:
	static const float MAX_SPEED;
	CVector2 m_position;
	CVector2 m_velocity;

	CVector2 m_gravity;
	float m_radius;

private:
	void CalculateLineCollision(CVector2 sweepEnd, CLineSegment& line, float& nearestDistanceSquared, CVector2& nearestPosition,
		CVector2& nearestPoint, CVector2& nearestNormal, CLineSegment*& nearestLine) const;
	bool CalculateFlipperCollision(CVector2 position, CFlipper& flipper, float nextPaddleAngle, float deltaTime, float timeOfCheck);
};