#pragma once
#include "table.h"


namespace TableHelpers
{
	void DecrementLineHits(CLineSegment& line);

	void FlipGravity(CLineSegment& line);

	void UpdateFlipperStates();

	void UpdateFlippers(float deltaTime);
}