#include "stdafx.h"

#include <math.h>

#include "App/AppSettings.h"
#include "table.h"
#include "TableHelpers.h"
#include "Vector2.h"

CTable::CTable()
{
	m_lineDefs[eLine_Hard] = CLineDefinition("Hard", 0.8f, 0.8f, 0.8f);
	m_lineDefs[eLine_Soft] = CLineDefinition("Soft", 0.4f, 0.3f, 0.1f, 0.35f);
	m_lineDefs[eLine_Power] = CLineDefinition("Power", 0.2f, 0.2f, 0.6f, 1.5f);
	m_lineDefs[eLine_Flipper] = CLineDefinition("Flipper", 0.5f, 0.9f, 0.1f);

	m_lineDefs[eLine_Flip] = CLineDefinition("Flip", 1.0f, 0.6f, 0.1f);
	m_lineDefs[eLine_Flip].m_hitCallback = &TableHelpers::FlipGravity;

	m_lineDefs[eLine_Score] = CLineDefinition("Score", 0.7f, 0.4f, 0.4f);
	
	m_lineDefs[eLine_1Hit] = CLineDefinition("1Hit", 0.9f, 0.2f, 0.2f);
	m_lineDefs[eLine_1Hit].m_hitCallback = &TableHelpers::DecrementLineHits;

	m_lineDefs[eLine_2Hit] = CLineDefinition("2Hit", 0.2f, 0.9f, 0.2f);
	m_lineDefs[eLine_2Hit].m_hitCallback = &TableHelpers::DecrementLineHits;

	m_lineDefs[eLine_3Hit] = CLineDefinition("3Hit", 0.2f, 0.2f, 0.9f);
	m_lineDefs[eLine_3Hit].m_hitCallback = &TableHelpers::DecrementLineHits;
}

CLineSegment::CLineSegment() : CLineSegment(0.0f, 0.0f, 0.0f, 0.0f, eLine_Flip)
{
}

CLineSegment::CLineSegment(float x1, float y1, float x2, float y2, LineType type)
{
	m_start.m_x = x1;
	m_start.m_y = y1;
	m_start.m_selected = false;
	m_end.m_x = x2;
	m_end.m_y = y2;
	m_end.m_selected = false;
	m_type = type;
	m_selected = false;
}


float CLineSegment::DistanceToLine(float x, float y)
{
	const float x0 = m_start.m_x;
	const float x1 = m_end.m_x;
	const float y0 = m_start.m_y;
	const float y1 = m_end.m_y;

	float dx = x1 - x0;
	float dy = y1 - y0;

	float linelengthSquared = dx*dx + dy*dy;
	float param = ((x - x0)*dx + (y - y0)*dy) / linelengthSquared;
	if (param > 1)
		param = 1;
	if (param < 0)
		param = 0;

	float nearestX = x0 + param*dx;
	float nearestY = y0 + param*dy;

	float distX = x - nearestX;
	float distY = y - nearestY;
	float distance = sqrt((distX*distX) + (distY*distY));
	return distance;
}

/* float CLineSegment::DistanceToInfiniteLine(float x, float y)
{
	// See https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
	const float x0 = m_start.m_x;
	const float x1 = m_end.m_x;
	const float y0 = m_start.m_y;
	const float y1 = m_end.m_y;

	float numerator = abs((y1 - y0)*x - (x1 - x0)*y + x1*y0 - y1*x0);
	float denomenator = sqrt((y1 - y0)*(y1 - y0) + (x1 - x0)*(x1 - x0));
	float distance = numerator / denomenator;
	
	return distance;
} */

bool CLineSegment::IsOnLine(float x, float y, float tolerance)
{
	return DistanceToLine(x,y) < tolerance;
}

CPoint::CPoint()
	: m_x(0.0f), m_y(0.0f)
{
}

CPoint::CPoint(float x, float y)
	: m_x(x), m_y(y)
{
}

float CPoint::DistanceToPoint(float x, float y)
{
	float dX = x - m_x;
	float dY = y - m_y;
	float distance = sqrt(dX*dX + dY*dY);

	return distance;
}

bool CPoint::IsOnPoint(float x, float y, float tolerance)
{
	return DistanceToPoint(x, y) < tolerance;
}

CPoint::operator CVector2() const
{
	return CVector2(m_x, m_y);
}

CLineDefinition::CLineDefinition(float restitution)
	: m_restitution(restitution)
{}

CLineDefinition::CLineDefinition(const char * name, float r, float g, float b, float restitution)
{
	m_name = name;
	m_Red = r;
	m_Green = g;
	m_Blue = b;
	m_restitution = restitution;
	m_hitCallback = nullptr;
}

CFlipper::CFlipper(float x, float y, float length, bool isRightFlipper, bool isInverted)
	: CFlipper(CPoint(x, y), length, isRightFlipper, isInverted)
{
}

CFlipper::CFlipper(CPoint position, float length, bool isRightFlipper, bool isInverted)
{
	m_isSelected = false;

	m_isInverted = isInverted;

	m_position = position;
	m_length = length;

	m_position.m_selected = false;
	m_isRightFlipper = isRightFlipper;
	
	RecalculateAngles(isRightFlipper, isInverted);

	m_angle = m_downAngle;
}

void CFlipper::RecalculateAngles()
{
	// 30 degrees down/up
	if (m_isRightFlipper)
	{
		if (m_isInverted)
		{
			m_downAngle = 150.0f;
			m_upAngle = 210.0f;
			m_flipDirection = 1;
		}
		else
		{
			m_downAngle = 210.0f;
			m_upAngle = 150.0f;
			m_flipDirection = -1;
		}
	}
	else
	{
		if (m_isInverted)
		{
			m_downAngle = 30.0f;
			m_upAngle = -30.0f;
			m_flipDirection = -1;
		}
		else
		{
			m_downAngle = -30.0f;
			m_upAngle = 30.0f;
			m_flipDirection = 1;
		}
	}
}

void CFlipper::RecalculateAngles(bool isRightFlipper, bool isInverted)
{
	m_isRightFlipper = isRightFlipper;
	m_isInverted = isInverted;

	RecalculateAngles();
}

float CFlipper::GetSpeedAtPoint(CVector2 point) const
{
	if (m_speed == 0.0f)
	{
		return 0.0f;
	}

	CVector2 position = m_position;
	CVector2 relativePoint = point - position;

	if (relativePoint.GetMagnitudeSquared() == 0.0f)
	{
		return 0.0f;
	}

	/*float radAngle = m_angle / 180.0f * PI;

	CVector2 direction = CVector2(cosf(radAngle),
	sinf(radAngle));*/

	float radius = relativePoint.GetMagnitude();
	// Tangent is perpendicular to line, so -1/line slope
	return fabsf(m_speed) * PI / 180.0f * radius;
}

CVector2 CFlipper::GetVelocityAtPoint(CVector2 point) const
{
	if (m_speed == 0.0f)
	{
		return CVector2(0.0f);
	}

	CVector2 position = m_position;
	CVector2 relativePoint = point - position;

	if (relativePoint.GetMagnitudeSquared() == 0.0f)
	{
		return CVector2(0.0f);
	}

	/*float radAngle = m_angle / 180.0f * PI;

	CVector2 direction = CVector2(cosf(radAngle),
		sinf(radAngle));*/

	float radius = relativePoint.GetMagnitude();
	// Tangent is perpendicular to line, so -1/line slope
	return (-1.0f / relativePoint).GetNormalized() * m_speed * PI / 180.0f * radius;
	// use m_speed to get the velocity of this thing
}

CVector2 CFlipper::GetEndpoint() const
{
	float radAngle = m_angle / 180.0f * PI;
	return (CVector2)m_position + CVector2(cosf(radAngle), sinf(radAngle)) * m_length;
}

CVector2 CFlipper::GetEndpoint(float angle) const
{
	float radAngle = angle / 180.0f * PI;
	return (CVector2)m_position + CVector2(cosf(radAngle), sinf(radAngle)) * m_length;
}

CLineSegment CFlipper::GetLineSegment() const
{
	CLineSegment line;
	line.m_type = eLine_Flipper;
	line.m_start = m_position;
	line.m_end = GetEndpoint();
	return line;
}
