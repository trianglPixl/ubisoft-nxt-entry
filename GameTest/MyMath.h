#pragma once
#ifndef PI
#define PI 3.14159265359f
#endif

class CVector2;

float biclamp(float a, float b, float x);

float clamp(float min, float max, float x);

float clamp01(float x);

template <typename T> int sign(T val) { return (T(0) < val) - (val < T(0)); }

float lerp(float a, float b, float x);

float inverseLerp(float a, float b, float x);

bool IsAngleBetweenDegrees(float a, float b, float x);

// Circle-line intersection adapted from http://ericleong.me/research/circle-line/

bool IsPointInBounds(const CVector2& point, const CVector2& p1, const CVector2& p2, float epsilon = 0.001f);

bool GetLineLineIntersection(CVector2 p1, CVector2 p2, CVector2 p3, CVector2 p4, CVector2& intersection);

CVector2 GetNearestLinePoint(CVector2 p1, CVector2 p2, CVector2 point);

bool CircleSweepLine(CVector2 start, CVector2 end, float radius, CVector2 p1, CVector2 p2,
	CVector2& endPosition, CVector2& lineCollisionPoint, CVector2& normal);

bool GetPointOnCircleTangentThroughPoint(CVector2 circleCenter, float radius, CVector2 point, CVector2& outPoint1, CVector2& outPoint2);