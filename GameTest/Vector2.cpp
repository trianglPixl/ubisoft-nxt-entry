#include "stdafx.h"
#include "Vector2.h"
#include "table.h"

CVector2::CVector2()
	: CVector2(0.0f, 0.0f)
{
}

CVector2::CVector2(float xy)
	: CVector2(xy, xy)
{
}


CVector2::CVector2(float x, float y)
	: x(x), y(y)
{
}

CVector2::~CVector2()
{
}

CVector2::operator CPoint() const
{
	return CPoint(x, y);
}
