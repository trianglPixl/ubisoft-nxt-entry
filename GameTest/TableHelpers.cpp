#include "stdafx.h"
#include "TableHelpers.h"

#include <algorithm>

#include "App/SimpleController.h"
#include "MyMath.h"
#include "Ball.h"

extern CTable* gTable;
extern CBall* gBall;

void TableHelpers::DecrementLineHits(CLineSegment & line)
{
	switch (line.m_type)
	{
	case eLine_3Hit:
	case eLine_2Hit:
		line.m_type = static_cast<LineType>(line.m_type - 1);
		break;
	case eLine_1Hit:
		gTable->m_lines.erase(std::remove_if(gTable->m_lines.begin(),
			gTable->m_lines.end(),
			[&line](CLineSegment& other) { return &other == &line; }));
		break;
	default:
		break;
	}
}

void TableHelpers::FlipGravity(CLineSegment & line)
{
	gBall->m_gravity = -gBall->m_gravity;
}

void TableHelpers::UpdateFlipperStates()
{
	gTable->m_stationaryFlippers.clear();

	// Full revolution in 1/5 of a second
	// 60 degrees in 1/30 of a second
	const float flipperAngularSpeed = 360.0f * 2.0f;

	CController controller = CSimpleControllers::GetInstance().GetController(0);

	for (auto& flipper : gTable->m_flippers)
	{
		float speed = flipperAngularSpeed;
		int flipDirection = flipper.m_flipDirection;
		if (flipper.m_isRightFlipper)
		{
			flipDirection *= controller.GetRightTrigger() > 0.25f ? 1 : -1;
		}
		else
		{
			flipDirection *= controller.GetLeftTrigger() > 0.25f ? 1 : -1;
		}

		flipper.m_speed = speed * flipDirection;
		if (flipDirection == 0.0f ||
			flipDirection > 0.0f && flipper.m_angle == fmaxf(flipper.m_downAngle, flipper.m_upAngle) ||
			flipDirection < 0.0f && flipper.m_angle == fminf(flipper.m_downAngle, flipper.m_upAngle))
		{
			gTable->m_stationaryFlippers.push_back(flipper.GetLineSegment());
		}
	}
}

void TableHelpers::UpdateFlippers(float deltaTime)
{
	deltaTime /= 1000.0f;

	for (auto& flipper : gTable->m_flippers)
	{
		flipper.m_angle += flipper.m_speed * deltaTime;
		flipper.m_angle = biclamp(flipper.m_downAngle, flipper.m_upAngle, flipper.m_angle);
	}
}
