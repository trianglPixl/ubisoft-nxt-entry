#pragma once

class CPoint;

class CVector2
{
public:
	static CVector2 Lerp(CVector2 start, CVector2 end, float t)
	{
		return start + (end - start) * t;
	}

public:
	CVector2();
	explicit CVector2(float xy);
	CVector2(float x, float y);
	~CVector2();

	operator CPoint() const;

	CVector2 operator-() const { return CVector2(-x, -y); }

	CVector2 operator+(const CVector2& other) const { return CVector2(x + other.x, y + other.y); }
	CVector2 operator-(const CVector2& other) const { return CVector2(x - other.x, y - other.y); }

	CVector2 operator*(const float& n) const { return CVector2(x * n, y * n); };
	CVector2 operator*(const CVector2& other) const { return CVector2(x * other.x, y * other.y); }

	CVector2 operator/(const float& n) const { return CVector2(x / n, y / n); };
	CVector2 operator/(const CVector2& other) const { return CVector2(x / other.x, y / other.y); }


	void operator+=(const CVector2& other) { x += other.x; y += other.y; }
	void operator-=(const CVector2& other) { x -= other.x; y -= other.y; }

	void operator*=(const float& n) { x *= n; y *= n; }
	void operator*=(const CVector2& other) { x *= other.x; y *= other.y; }

	void operator/=(const float& n) { x /= n; y /= n; }
	void operator/=(const CVector2& other) { x /= other.x; y /= other.y; }

	void operator=(const CVector2& other) { x = other.x; y = other.y; }
	bool operator==(const CVector2& other) const { return x == other.x && y == other.y; }
	bool operator!=(const CVector2& other) const { return !(*this == other); }

	float Dot(const CVector2& other) const { return x * other.x + y * other.y; }
	CVector2 Reflect(const CVector2& normal) const { return *this - normal * 2.0f * this->Dot(normal); }

	float GetMagnitudeSquared() const { return x*x + y*y; }
	float GetMagnitude() const { return sqrtf(x*x + y*y); }
	float GetDistance(const CVector2& other) const { return (*this - other).GetMagnitude(); }
	float GetDistanceSquared(const CVector2& other) const { return (*this - other).GetMagnitudeSquared(); }

	CVector2 GetNormalized() const { return *this / GetMagnitude(); }
	void Normalize() { *this /= GetMagnitude(); }
	
public:
	float x;
	float y;
};

inline CVector2 operator*(const float& a, const CVector2& b) { return CVector2(a * b.x, a * b.y); }

inline CVector2 operator/(const float& a, const CVector2& b) { return CVector2(a / b.x, a / b.y); }