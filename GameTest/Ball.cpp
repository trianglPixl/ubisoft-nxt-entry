#include "stdafx.h"
#include <Windows.h>
#include "Ball.h"
#include "table.h"
#include "MyMath.h"
#include "App/App.h"

const float CBall::MAX_SPEED = 1000.0f;

extern CTable* gTable;
extern const char* gStatusMessage;

CBall::CBall(CVector2 position, float radius, CVector2 gravity)
	: m_position(position), m_radius(radius), m_gravity(gravity), m_velocity(0.0f, 0.0f)
{
}


CBall::~CBall()
{
}

void CBall::Update(float deltaTime)
{
	//CVector2 result1, result2;
	//GetPointOnCircleTangentThroughPoint(CVector2(0.0f), sqrtf(4.0f), CVector2(5, 3), result1, result2);

	//deltaTime = 16.0f;
	deltaTime /= 1000.0f;
	// I probably want to update using a fixed time step but I'll do that in my physics step system
	// Caveat: gravity applies over the whole step
	//  but collisions reflect that applied gravity partway through
	m_velocity += m_gravity * deltaTime;

	float timeRemaining = deltaTime;

	int iterations = 0;

	// We need this to prevent hitting the same line twice in the same step
	//  Doing that reduces some tunnelling at corners
	CLineSegment* lastLineHit = nullptr;

	// sanity check - 10 collisions per frame to prevent an unlikely infinite loop
	while (timeRemaining > 0.0f && iterations < 10)
	{
		iterations++;

		if (m_velocity.GetMagnitudeSquared() == 0.0f)
		{
			return;
		}

		if (m_velocity.GetMagnitudeSquared() > MAX_SPEED * MAX_SPEED)
		{
			m_velocity = m_velocity.GetNormalized() * MAX_SPEED;
		}

		CVector2 deltaPosition = m_velocity * timeRemaining;

		CVector2 sweepEnd = m_position + deltaPosition;

		float nearestDistanceSquared = INFINITY;
		CVector2 nearestPosition;
		CVector2 nearestPoint;
		CVector2 nearestNormal;
		CLineSegment* nearestLine = nullptr;

		for (auto& line : gTable->m_lines)
		{
			if (&line == lastLineHit)
			{
				continue;
			}

			CalculateLineCollision(sweepEnd, line,
				nearestDistanceSquared, nearestPosition, nearestPoint, nearestNormal, nearestLine);
		}

		for (auto& line : gTable->m_stationaryFlippers)
		{
			if (&line == lastLineHit)
			{
				continue;
			}

			CalculateLineCollision(sweepEnd, line,
				nearestDistanceSquared, nearestPosition, nearestPoint, nearestNormal, nearestLine);
		}


		if (nearestDistanceSquared < deltaPosition.GetMagnitudeSquared())
		{
			// We have a collision - move to the point of contact, reflect velocity along the hit normal and check for more collisions in the next loop iteration

			lastLineHit = nearestLine;

			float distanceToCollision = sqrtf(nearestDistanceSquared);
			timeRemaining -= timeRemaining * (distanceToCollision / deltaPosition.GetMagnitude());

			m_velocity = m_velocity.Reflect(nearestNormal);
			CVector2 velocityAlongNormal = m_velocity.Dot(nearestNormal) * nearestNormal;
			CVector2 velocityPerpendicularToNormal = m_velocity - velocityAlongNormal;

			CLineDefinition& lineDef = gTable->m_lineDefs[nearestLine->m_type];
			if (lineDef.m_hitCallback != nullptr)
			{
				lineDef.m_hitCallback(*nearestLine);
			}

			velocityAlongNormal *= lineDef.m_restitution;
			m_velocity = velocityAlongNormal + velocityPerpendicularToNormal;
			if (m_velocity.GetMagnitudeSquared() > MAX_SPEED * MAX_SPEED)
			{
				m_velocity = m_velocity.GetNormalized() * MAX_SPEED;
			}

			m_position = nearestPosition;

			//m_position += m_velocity * timeRemaining;
			//char messageBuffer[256];
			//snprintf(messageBuffer, 255, "%f, %f\n", velocityPerpendicularToNormal.x, velocityAlongNormal.y);
			//OutputDebugStringA(messageBuffer);
		}
		else
		{
			// Loop through flippers and do collision with the startpoint and endpoint of the sweep
			// One person is not enough to simulate collisions between a moving circle and a rotating line in a weekend
			// I'm also going to assume one flipper collision per frame since there're a lot fewer flippers than lines
			bool hasFlipperContact = false;

			for (auto& flipper : gTable->m_flippers)
			{
				float currentAngle = flipper.m_angle;
				float nextAngle = currentAngle + flipper.m_speed * timeRemaining;
				hasFlipperContact = CalculateFlipperCollision(m_position, flipper, nextAngle, timeRemaining, 0.0f);
				if (hasFlipperContact)
				{
					break;
				}

				hasFlipperContact = CalculateFlipperCollision(sweepEnd, flipper, nextAngle, timeRemaining, timeRemaining);
				if (hasFlipperContact)
				{
					break;
				}
			}

			if (!hasFlipperContact)
			{
				m_position = sweepEnd;
			}

			break;
		}
	}

	static char* deathsBuffer = nullptr;
	static int deathCount = 0;

	if (!IsPointInBounds(m_position, CVector2(0.0f), CVector2(1024.0f, 768.0f)))
	{
		m_position = gTable->m_spawnPosition;
		m_velocity = CVector2(0.0f);
		
		deathCount++;
		if (deathsBuffer != nullptr)
		{
			delete[] deathsBuffer;
			deathsBuffer = nullptr;
		}

		int length = snprintf(nullptr, 0, "Deaths: %d", deathCount);
		deathsBuffer = new char[length + 1];
		snprintf(deathsBuffer, length + 1, "Deaths: %d", deathCount);

		gStatusMessage = deathsBuffer;
	}
}

void CBall::CalculateLineCollision(CVector2 sweepEnd, CLineSegment& line, float& nearestDistanceSquared, CVector2& nearestPosition, CVector2& nearestPoint, CVector2& nearestNormal, CLineSegment*& nearestLine) const
{
	CVector2 outPosition;
	CVector2 outPoint;
	CVector2 outNormal;

	CVector2 lineStart = CVector2(line.m_start.m_x, line.m_start.m_y);
	CVector2 lineEnd = CVector2(line.m_end.m_x, line.m_end.m_y);

	bool isHit = CircleSweepLine(m_position, sweepEnd, m_radius,
		lineStart, lineEnd,
		outPosition, outPoint, outNormal);

	if (isHit)
	{
		CVector2 collisionDelta = (outPosition - m_position);
		float hitDistanceSquared = collisionDelta.GetMagnitudeSquared();

		if (hitDistanceSquared < nearestDistanceSquared)
		{
			nearestDistanceSquared = hitDistanceSquared;
			nearestPosition = outPosition;
			nearestPoint = outPoint;
			nearestNormal = outNormal;
			nearestLine = &line;
		}
	}
}

bool CBall::CalculateFlipperCollision(CVector2 position, CFlipper& flipper, float nextPaddleAngle, float deltaTime, float timeOfCheck)
{
	float radii = flipper.m_length + m_radius;
	if (position.GetDistanceSquared(flipper.m_position) > radii * radii)
	{
		return false;
	}

	float currentAngle = flipper.m_angle;
	
	nextPaddleAngle = biclamp(flipper.m_downAngle, flipper.m_upAngle, nextPaddleAngle);

	// Stationary flippers should be added to a stationary flipper list
	if (currentAngle == nextPaddleAngle)
	{
		return false;
	}

	CVector2 flipperPos = flipper.m_position;

	// If we're inside of the flipper, get out
	CVector2 flipperEndpoint = flipper.GetEndpoint(nextPaddleAngle);
	CVector2 nearestPointAlongFlipper = GetNearestLinePoint(flipperPos, flipperEndpoint, position);

	CVector2 flipperDelta = (flipperEndpoint - flipperPos).GetNormalized();

	// This is a bit of a hack but it's close enough
	//bool isBackwards = ;
	if (flipperDelta.Dot(nearestPointAlongFlipper - flipperPos) < 0)
	{
		nearestPointAlongFlipper = flipperPos;
	}

	CVector2 deltaFromPoint = position - nearestPointAlongFlipper;

	if (deltaFromPoint.GetMagnitudeSquared() < m_radius * m_radius)
	{
		CVector2 normal = deltaFromPoint.GetNormalized();
		m_position = nearestPointAlongFlipper + normal * m_radius;

		//OutputDebugString(L"Knocked ball out of flipper\n");

		float speedAtPoint = flipper.GetSpeedAtPoint(nearestPointAlongFlipper);

		// This is pretty wrong but I need to knock the ball out of the flipper's way
		m_velocity = m_velocity.Reflect(normal);
		CVector2 velocityAlongNormal = m_velocity.Dot(normal) * normal;
		m_velocity -= velocityAlongNormal;
		m_velocity += speedAtPoint * normal;

		float timeToFlipperContact = timeOfCheck;

		m_position += m_velocity * (deltaTime - timeToFlipperContact);

		return true;
	}

	CVector2 contact1, contact2;
	bool isValid = GetPointOnCircleTangentThroughPoint(position, m_radius, flipperPos,
		contact1, contact2);

	if (!isValid)
	{
		return false;
	}

	CVector2 relativeContact1 = contact1 - flipperPos;
	CVector2 relativeContact2 = contact2 - flipperPos;

	int direction = sign(flipper.m_speed);

	float angle1 = atan2(relativeContact1.y, relativeContact1.x) / PI * 180.0f;
	float angle2 = atan2(relativeContact2.y, relativeContact2.x) / PI * 180.0f;

	float minAngle = fminf(currentAngle, nextPaddleAngle);
	float maxAngle = fmaxf(currentAngle, nextPaddleAngle);

	bool isAngle1Valid = IsAngleBetweenDegrees(currentAngle, nextPaddleAngle, angle1);
	bool isAngle2Valid = IsAngleBetweenDegrees(currentAngle, nextPaddleAngle, angle2);

	float flipperAngleAtContact;
	CVector2 contactPoint;

	if (isAngle1Valid && isAngle2Valid)
	{
		bool isAngle1Less = angle1 < angle2;

		if (direction == -1)
		{
			isAngle1Less = !isAngle1Less;
		}

		if (isAngle1Less)
		{
			contactPoint = contact1;
			flipperAngleAtContact = angle1;
		}
		else
		{
			contactPoint = contact2;
			flipperAngleAtContact = angle2;
		}
	}
	else if (isAngle1Valid)
	{
		contactPoint = contact1;
		flipperAngleAtContact = angle1;
	}
	else if (isAngle2Valid)
	{
		contactPoint = contact2;
		flipperAngleAtContact = angle2;
	}
	else
	{
		return false;
	}
	
	CVector2 normal = (position - contactPoint).GetNormalized();
	float speedAtPoint = flipper.GetSpeedAtPoint(contactPoint);

	// This is pretty wrong but I need to knock the ball out of the flipper's way
	m_position = position;
	m_velocity = m_velocity.Reflect(normal);
	CVector2 velocityAlongNormal = m_velocity.Dot(normal) * normal;
	m_velocity -= velocityAlongNormal;
	m_velocity += speedAtPoint * normal;

	float timeToFlipperContact = clamp01(inverseLerp(currentAngle, nextPaddleAngle, flipperAngleAtContact));
	timeToFlipperContact *= timeOfCheck;

	m_position += m_velocity * (deltaTime - timeToFlipperContact);

	return true;
}
