#pragma once

class CPoint;

enum EditorState
{
	eState_Edit,
	eState_Add,
	eState_Save,
};

namespace Editor
{
	void DrawBoxAroundPoint(const CPoint& point, float size);
	void DrawFlipper(const CFlipper& flipper);

	EditorState UpdateMoveLine(float x, float y);
	EditorState UpdateAddLine(float x, float y);
	EditorState Save(const char* fileName);

	void DeselectAll();

	void Load(const char* fileName);
}
