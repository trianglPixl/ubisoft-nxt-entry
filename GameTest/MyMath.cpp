#include "stdafx.h"

#include <math.h>
#include "MyMath.h"
#include "Vector2.h"

float biclamp(float a, float b, float x)
{
	return clamp(fminf(a, b), fmaxf(a, b), x);
}

float clamp(float min, float max, float x)
{
	return fminf(fmaxf(min, x), max);
}

float clamp01(float x)
{
	return clamp(0.0f, 1.0f, x);
}

float lerp(float a, float b, float x)
{
	return a + (b - a) * x;
}

float inverseLerp(float a, float b, float x)
{
	if (a == b)
	{
		return 0.0f;
	}
	return (x - a) / (b - a);
}

// https://stackoverflow.com/questions/12234574/calculating-if-an-angle-is-between-two-angles
bool IsAngleBetweenDegrees(float a, float b, float x)
{
	float minAngle = fminf(a, b);
	float maxAngle = fmaxf(a, b);

	float angleWindow = (maxAngle - minAngle) * 0.5f;
	float middleAngle = (minAngle + maxAngle) / 2.0f;

	float deltaAngle = fmodf(middleAngle - x + 180.0f + 360.0f, 360.0f) - 180.0f;

	return deltaAngle <= angleWindow && deltaAngle >= -angleWindow;
}

bool IsPointInBounds(const CVector2& point, const CVector2& p1, const CVector2& p2, float epsilon)
{
	return point.x >= fminf(p1.x, p2.x) - epsilon &&
		point.x <= fmaxf(p1.x, p2.x) + epsilon &&
		point.y >= fminf(p1.y, p2.y) - epsilon &&
		point.y <= fmaxf(p1.y, p2.y) + epsilon;
}

bool GetLineLineIntersection(CVector2 p1, CVector2 p2, CVector2 p3, CVector2 p4, CVector2& intersection)
{
	float a1 = p2.y - p1.y;
	float b1 = p1.x - p2.x;
	float a2 = p4.y - p3.y;
	float b2 = p3.x - p4.x;

	float c1 = a1 * p1.x + b1 * p1.y;
	float c2 = a2 * p3.x + b2 * p3.y;

	float determinant = a1 * b2 - a2 * b1;

	if (determinant == 0)
	{
		return false;
	}

	float x = (c1 * b2 - b1 * c2) / determinant;
	float y = (a1 * c2 - a2 * c1) / determinant;

	bool isXValid = x >= fminf(p1.x, p2.x) &&
		x <= fmaxf(p1.x, p2.x) &&
		x >= fminf(p3.x, p4.x) &&
		x <= fmaxf(p3.x, p4.x);
	bool isYValid = y >= fminf(p1.y, p2.y) &&
		y <= fmaxf(p1.y, p2.y) &&
		y >= fminf(p3.y, p4.y) &&
		y <= fmaxf(p3.y, p4.y);

	// Set the intersection even if the intersection lies outside of the segments
	intersection = CVector2(x, y);

	if (isXValid && isYValid)
	{
		return true;
	}

	return false;
}

CVector2 GetNearestLinePoint(CVector2 p1, CVector2 p2, CVector2 point)
{
	float a = p2.y - p1.y;
	float b = p1.x - p2.x;

	float c1 = a * p1.x + b * p1.y;
	float c2 = -b * point.x + a * point.y;

	float determinant = a * a + b * b;

	// The point is on the line
	if (determinant == 0)
	{
		return point;
	}

	float x = (a * c1 - b * c2) / determinant;
	float y = (a * c2 + b * c1) / determinant;

	return CVector2(x, y);
}

bool CircleSweepLine(CVector2 start, CVector2 end, float radius, CVector2 p1, CVector2 p2,
	CVector2& endPosition, CVector2& contactPoint, CVector2& normal)
{
	CVector2 direction = (end - start).GetNormalized();

	// Where the displacement vector intersects with the line segment
	CVector2 displacementIntersection;
	bool isDisplacementIntersecting = GetLineLineIntersection(start,
		end, p1, p2, displacementIntersection);

	CVector2 pointNearestEndPosition = GetNearestLinePoint(p1, p2, end);
	CVector2 p1NearestDisplacement = GetNearestLinePoint(start, end, p1);
	CVector2 p2NearestDisplacement = GetNearestLinePoint(start, end, p2);

	if (isDisplacementIntersecting ||
		pointNearestEndPosition.GetDistance(end) <= radius && IsPointInBounds(pointNearestEndPosition, p1, p2) ||
		direction.Dot(p1NearestDisplacement - start) >= 0.0f && p1NearestDisplacement.GetDistance(p1) <= radius && IsPointInBounds(p1NearestDisplacement, start, end, radius) ||
		direction.Dot(p2NearestDisplacement - start) >= 0.0f && p2NearestDisplacement.GetDistance(p2) <= radius && IsPointInBounds(p2NearestDisplacement, start, end, radius))
	{
		
		float startToIntersectionDistance = (start - displacementIntersection).GetMagnitude();
		CVector2 pointNearestStartPosition = GetNearestLinePoint(p1, p2, start);
		float startToNearestDistance = (start - pointNearestStartPosition).GetMagnitude();
		CVector2 positionAtCollision = displacementIntersection -
			radius * (startToIntersectionDistance / startToNearestDistance) * direction;

		CVector2 nearestLinePointToCollision = GetNearestLinePoint(p1, p2, positionAtCollision);

		// Check if that point is on the line
		if (IsPointInBounds(nearestLinePointToCollision, p1, p2))
		{
			// We collided with the line
			contactPoint = nearestLinePointToCollision;
		}
		else
		{
			// We collided with an endpoint

			float endpointDistance;

			float distanceSquaredToP1 = (nearestLinePointToCollision - p1).GetMagnitudeSquared();
			float distanceSquaredToP2 = (nearestLinePointToCollision - p2).GetMagnitudeSquared();
			if (distanceSquaredToP1 <
				distanceSquaredToP2)
			{
				// Collided with p1
				endpointDistance = sqrtf(distanceSquaredToP1);
				contactPoint = p1;
			}
			else
			{
				//Collided with p2
				endpointDistance = sqrtf(distanceSquaredToP2);
				contactPoint = p2;
			}

			CVector2 nearestPointToEndpoint = GetNearestLinePoint(start, end, contactPoint);
			endpointDistance = (nearestPointToEndpoint - contactPoint).GetMagnitude();

			float distanceToMoveBack = sqrtf(radius * radius - endpointDistance * endpointDistance);
			float distanceTravelled = (start - end).GetMagnitude();
			positionAtCollision = nearestPointToEndpoint - distanceToMoveBack * direction;
		}

		endPosition = positionAtCollision;

		normal = (positionAtCollision - contactPoint).GetNormalized();
		return true;
	}

	return false;
}

bool GetPointOnCircleTangentThroughPoint(CVector2 circleCenter, float radius, CVector2 point, CVector2& outPoint1, CVector2& outPoint2)
{
	// My algebra to solve this problem is in <repository root>/Documentation/Line Through Point Tangential to Circle.pdf

	if (point == circleCenter)
	{
		return false;
	}

	CVector2 p = point - circleCenter;


	float radius2 = radius * radius;

	// (p.x^2 + p.y^2) * x - 2 * r^2 * p.x * x + r^4 - p.y^2 * r^2 = 0
	// (a            )    (b              )   (c                  )

	float a = p.Dot(p);
	float b = -2.0f * radius2 * p.x;
	float c = radius2 * radius2 - p.y * p.y * radius2;

	// Quadratic formula time!
	// a is always positive or 0 and if it's 0, we returned at the start
	// the descriminant should be > 0 unless I forgot about an edge case

	float discriminant = b * b - 4.0f * a * c;

	// hopefully, this doesn't happen
	// (probably a small chance of it happening when the point is inside the circle)
	if (discriminant <= 0)
	{
		return false;
	}

	float sqrtDiscriminant = sqrtf(discriminant);

	float x1 = (-b + sqrtDiscriminant) / (2.0f * a);
	float x2 = (-b - sqrtDiscriminant) / (2.0f * a);

	float y1 = (radius2 - x1 * p.x) / p.y;
	float y2 = (radius2 - x2 * p.x) / p.y;

	outPoint1 = circleCenter + CVector2(x1, y1);
	outPoint2 = circleCenter + CVector2(x2, y2);

	return true;
}
